export const focus = {
  inserted(el: HTMLElement) {
    el.focus()
    if (el instanceof HTMLInputElement) {
      el.select()
    }
  }
}

export default focus
