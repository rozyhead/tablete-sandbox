export interface Card {
  id: string
  title: string
  listId: string
}

export default Card
