import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import './bootstrap-custom.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
