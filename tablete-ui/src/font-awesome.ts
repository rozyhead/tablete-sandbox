import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEllipsisH, faLock, faPlus } from '@fortawesome/free-solid-svg-icons'
import { faStar } from '@fortawesome/free-regular-svg-icons'
const fontawesome = require('@fortawesome/vue-fontawesome')

library.add(faEllipsisH, faLock, faPlus, faStar)

Vue.component('fa-icon', fontawesome.FontAwesomeIcon)
